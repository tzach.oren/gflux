package coreflex.Gflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GfluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(GfluxApplication.class, args);
	}

}
