package coreflex.Gflux.fetcher;

import coreflex.Gflux.types.Application;
import coreflex.Gflux.types.ApplicationCategory;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class ApplicationCategoryFetcher {
    @Autowired
    private ReactiveMongoTemplate mongoTemplate;

    public Flux<ApplicationCategory> ApplicationCategory(DataFetchingEnvironment env){
        return mongoTemplate.findAll(ApplicationCategory.class,"application_category");
    }
}
