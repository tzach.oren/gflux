package coreflex.Gflux.fetcher;

import coreflex.Gflux.types.Application;
import coreflex.Gflux.types.ApplicationCategory;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class ApplicationFetcher {
    @Autowired
    private ReactiveMongoTemplate mongoTemplate;

    public Flux<Application> getApplications(DataFetchingEnvironment env){
        Query query = null;

        if(env.getArguments()!=null && !env.getArguments().isEmpty()){
            //TODO
            //query = new Query();
        }

        Flux<Application> applicationFlux;

        if(query == null){
            applicationFlux = mongoTemplate.findAll(Application.class);
        } else {
            applicationFlux = mongoTemplate.find(query,Application.class);
        }

        // TODO ADD categoty

        return  applicationFlux;
    }
}
