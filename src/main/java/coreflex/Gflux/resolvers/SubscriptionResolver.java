package coreflex.Gflux.resolvers;

import coreflex.Gflux.fetcher.ApplicationCategoryFetcher;
import coreflex.Gflux.fetcher.ApplicationFetcher;
import coreflex.Gflux.types.Application;
import coreflex.Gflux.types.ApplicationCategory;
import graphql.kickstart.tools.GraphQLSubscriptionResolver;
import graphql.schema.DataFetchingEnvironment;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SubscriptionResolver implements GraphQLSubscriptionResolver {
    @Autowired
    private ApplicationFetcher applicationFetcher;

    @Autowired
    private ApplicationCategoryFetcher categoryFetcher;

    Publisher<List<Application>> application(DataFetchingEnvironment env) {
        return applicationFetcher.getApplications(env).collectList();
    }

    Publisher<List<ApplicationCategory>> category(DataFetchingEnvironment env) {
        return categoryFetcher.ApplicationCategory(env).collectList();
    }

}