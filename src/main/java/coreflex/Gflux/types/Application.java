package coreflex.Gflux.types;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;
@Data
@Document(collation = "application")
public class Application {
    @Id
    private String id;

    @Field("name")
    private String name;

    @Transient
    private List<ApplicationCategory> applicationCategories;
}
