package coreflex.Gflux.types;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collation = "application_category")
public class ApplicationCategory {
    @Id
    private String id;

    @Field("name")
    private String name;
}
